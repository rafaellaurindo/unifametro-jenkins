package br.edu.unifametro.testepipe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class TestepipeApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestepipeApplication.class, args);
	}

	@GetMapping("/")
	public String home(){
		return "Arielton Nunes - Paixão por ensinar :)";
		
	}
}
